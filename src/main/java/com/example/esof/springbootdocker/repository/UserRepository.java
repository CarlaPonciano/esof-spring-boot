package com.example.esof.springbootdocker.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.esof.springbootdocker.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	
	List<User> findBySurname(String surname);
	
	User findById(int id);

}
