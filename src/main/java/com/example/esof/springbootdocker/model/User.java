package com.example.esof.springbootdocker.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

//Define que é uma entidade
@Entity
//Define qual é o nome da entidade
@Table(name = "user")
public class User {
	
	//Vai gerar o ID automaticamente, sem duplicados
	@Id
	@GeneratedValue
	private Integer userID;
	
	private String workCompany;
	
	private String firstName;
	
	private String surname;
	
	public User() {}
	
	public User (String name, String lastname) {
		super();
		firstName = name;
		surname = lastname;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getWorkCompany() {
		return workCompany;
	}

	public void setWorkCompany(String workCompany) {
		this.workCompany = workCompany;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
}
