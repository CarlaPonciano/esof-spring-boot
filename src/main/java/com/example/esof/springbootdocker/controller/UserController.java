package com.example.esof.springbootdocker.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.esof.springbootdocker.model.User;
import com.example.esof.springbootdocker.service.UserService;
import com.google.gson.Gson;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/users/create")
	public ResponseEntity<String> createUser() {
		List<User> user = userService.createUser();
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		//return ResponseEntity.created(location).build();
		//pega o header da chamada - status e body
		ResponseEntity<Object> header = ResponseEntity.created(location).build();
		String status = header.getStatusCode().toString();
		String body = new Gson().toJson(user);
		
		return ResponseEntity.created(location).header("StatusCode", status).body(body);
	}
	
	@GetMapping("/users/{userId}")
	@ResponseBody
	public ResponseEntity<String> retrieveUser(@PathVariable Integer userId) {
		User user = userService.retrieveUser(userId);
		String body = new Gson().toJson(user);
		return new ResponseEntity<>(body, HttpStatus.OK);
	}

}
