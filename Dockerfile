FROM openjdk:11
RUN addgroup --system spring && adduser --system spring --ingroup spring
RUN apt-get -y update
RUN apt-get install curl
RUN apt-get install sudo
USER spring:spring
ARG DEPENDENCY=target/dependency
EXPOSE 8080
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT [ "java", "-cp", "app:app/lib/*", "com.example.esof.springbootdocker.SpringBootDockerApplication" ]