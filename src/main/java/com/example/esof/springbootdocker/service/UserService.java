package com.example.esof.springbootdocker.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.esof.springbootdocker.model.User;
import com.example.esof.springbootdocker.repository.UserRepository;

//@Component
@Service
public class UserService {
	
	//Injeção de dependência
	@Autowired
	private UserRepository repository;
	
	public List<User> createUser() {
		List<User> users = new ArrayList<User>();
		List<User> savedUsers = new ArrayList<User>();
		
		users.add(new User("Carla", "Ponciano"));
		users.add(new User("Amanda", "Santos"));
		users.add(new User("Lavínia", "Félix"));
		
		Iterable<User> itrStudents = repository.saveAll(users);
		itrStudents.forEach(savedUsers::add);
		
		return savedUsers;
	}
	
	public User retrieveUser (Integer userId) {
		return repository.findById(userId).orElse(new User());
	}

}
